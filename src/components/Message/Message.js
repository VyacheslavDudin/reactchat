import React from 'react';
import './Message.css';

export default function Message(props) {
    let { avatar, text, user, msgId, createdAt, currentUser, onEdit, onLikeMessage, onDeleteMessage, likesCount } = props;
    createdAt = (new Date(createdAt)).toLocaleTimeString();
    if (currentUser !== user) {
        return (
            <div className="message">
                <div className="message-main">
                    <img className="avatar" src={avatar} alt="avatar"/>
                    <div className="message-body">
                        <span className="message-author">{user}</span>
                        <p className="message-text">{text}</p>
                    </div>
                </div>
                <div className="message-toolbar">
                    <i className="far fa-thumbs-up"onClick={() => onLikeMessage(msgId, currentUser)}>{likesCount}</i>
                    <span>{createdAt}</span>
                </div>      
            </div>
        );
    }
    else {
        return (
            <div className="message message_right">
                <div className="message-main">
                    <div className="message-body">
                        <p className="message-text">{text}</p>
                    </div>
                </div>
                <div className="message-toolbar">
                    <i className="far fa-thumbs-up"onClick={() => onLikeMessage(msgId, currentUser)}>{likesCount}</i>
                    <i className="far fa-edit" onClick={() => onEdit(msgId, text)}></i>
                    <i className="far fa-trash-alt" onClick={() => onDeleteMessage(msgId)}></i>
                    <span>{createdAt}</span>
                </div>      
            </div>
        );
    }
    
}