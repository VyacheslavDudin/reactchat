import React from 'react';
import './MessageInput.css';


export default function MessageInput({ onSendMessage, isEditMode, onEditMode }) {
    const onSend = () => {
        const newMessage = document.querySelector('.message__input').value;
        if (!newMessage || !newMessage.trim()) return;
        document.querySelector('.message__input').value = '';
        onSendMessage(newMessage); 
    };
    const onEdit = () => {
        const newMessage = document.querySelector('.message__input').value;
        if (!newMessage || !newMessage.trim()) return;
        document.querySelector('.message__input').value = '';
        onEditMode(newMessage); 
    }
    return (
        <div className="message__input-form">
            <textarea
                className="message__input"
                placeholder="Message"
                resize="none"
            >
            </textarea>
            <button className="button-send" onClick={isEditMode ? onEdit : onSend}><i className="fas fa-arrow-circle-up"></i></button>
        </div>
       
    );
}