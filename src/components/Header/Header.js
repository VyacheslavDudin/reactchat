import React from 'react';
import './Header.css';

export default function Header(props) {
    const { messagesCount, participantsCount, lastMessageDate } = props;
    const dateWithoutWeekDay = lastMessageDate.toDateString().substr(lastMessageDate.toDateString().indexOf(' '));
    const [,month, day, year] = dateWithoutWeekDay.split(' ');
    const time = `${lastMessageDate.getHours() + ':' + lastMessageDate.getMinutes()}`;
    let formattedDate = [day, month, year, time].join(' ');
    return (
        <div className="header">
            <div className="header__main-info">
                <span className="font-bold">My chat</span>
                <span className="font-italic">{participantsCount} participants</span>
                <span className="font-italic">{messagesCount} messages</span>
            </div>
            <span>last message at {formattedDate}</span>
        </div>
    );
}