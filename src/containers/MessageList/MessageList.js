import React, { useState } from 'react';
import './MessageList.css';
import Message from '../../components/Message/Message';
import MessageInput from '../../components/MessageInput/MessageInput';

export default function MessageList(props) {
    let { messages, currentUser, onSendMessage, onEditMessage, onLikeMessage, onDeleteMessage, likeRepository } = props;
    const [msgId, setMsgID] = useState('');
    const isEditMode = msgId ? true : false;
    const onEditMode = newMessage => {
        const icon = document.querySelector('.button-send > i');
        icon.classList.add('fa-arrow-circle-up');
        icon.classList.remove('fa-check-circle');
        onEditMessage(msgId, newMessage);
        setMsgID('')
    }
    const onEdit = (id, text) => {
        document.querySelector('.message__input').value = text;
        const icon = document.querySelector('.button-send > i');
        icon.classList.remove('fa-arrow-circle-up');
        icon.classList.add('fa-check-circle');
        setMsgID(id);
    }
    
    messages = messages.map(msg => 
        <Message 
            avatar={msg.avatar}
            text={msg.text}
            user={msg.user}
            createdAt={msg.createdAt}
            currentUser={currentUser}
            key={msg.id}
            msgId={msg.id}
            onEdit={(id, text) => onEdit(id, text)}
            onDeleteMessage={id => onDeleteMessage(id)}
            onLikeMessage={(id, userId) => onLikeMessage(id, userId)}
            likesCount={likeRepository.filter(el => el.id ===msg.id).length}
        />)
    return (
        <>
            <div className="messages">
                {messages}
            </div>
            <MessageInput
                onSendMessage={newMessage => onSendMessage(newMessage)}
                isEditMode={isEditMode}
                onEditMode={newMessage => onEditMode(newMessage)}
            />
        </>
    );
}