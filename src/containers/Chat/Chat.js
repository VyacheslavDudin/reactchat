import React, { Component } from 'react';
import './Chat.css';
import Header from './../../components/Header/Header';
import Spinner from './../../components/Spinner/Spinner';
import MessageList from '../MessageList/MessageList';

class Chat extends Component {
  constructor(props) {
    super(props);
    this.apiPath = 'https://edikdolynskyi.github.io/react_sources/messages.json';
    this.state = {
      messages: [],
      isMessageListLoaded: false,
      currentUser: {
        user:'Ben',
        userId: "5328dba1-1b8f-11e8-9629-c7eca82aa7bd",
        avatar: "https://www.aceshowbiz.com/images/photo/tom_pelphrey.jpg",
      },
      likeRepository: [],
      currentMaxId: 1
    };

    this.loadMessages = this.loadMessages.bind(this);
    this.sendMessage = this.sendMessage.bind(this);
    this.generateId = this.generateId.bind(this);
    this.editMessage = this.editMessage.bind(this);
    this.deleteMessage = this.deleteMessage.bind(this);
    this.likeMessage = this.likeMessage.bind(this);
  }

  componentDidMount() {
    this.loadMessages();
  }

  loadMessages() {
    fetch(this.apiPath)
      .then(response => response.json())
      .then(messages => this.setState({ messages, isMessageListLoaded: true }));
  }

  generateId() {
    this.setState(({currentMaxId}) => ({ currentMaxId: currentMaxId + 1 }) );
    return this.state.currentMaxId;
  }

  sendMessage (newMessage) {
    const { avatar, user, userId } = this.state.currentUser;
    const newMessageObj = {
      text: newMessage,
      avatar,
      createdAt: new Date().toISOString(),
      editedAt: '',
      user,
      userId,
      id: this.generateId()
    };
    this.setState( ({ messages }) => ({ messages: [...messages, newMessageObj] }));
  }

  likeMessage(id, userId) {
    this.setState( ({ likeRepository }) => {
      if (likeRepository.some(el => el.id === id && el.userId === userId)) {
        return ({ likeRepository: likeRepository.filter(el => el.userId !== userId || el.id !==id) });
      }
      else {
        return { likeRepository: [...likeRepository, { id, userId }] }
      }
    });
  }

  editMessage(id, newMessage) {
    this.setState( ({messages}) => ({messages: messages.map(msg => {
      if (msg.id === id) {
        msg.text = newMessage;
        msg.editedAt = new Date().toISOString();
      }
      return msg;
    }) }) );

  }

  deleteMessage(id) {
    this.setState( ({messages, likeRepository}) => (
      {
        messages: messages.filter(msg => msg.id !== id),
        likeRepository: likeRepository.filter(el => el.id !== id)
      }
    ));
  }

  render() {
    let { isMessageListLoaded:isLoaded, messages, currentUser, likeRepository } = this.state;
    const messagesCount = messages.length;
    const participants = new Set();
    messages.forEach(msg => participants.add(msg.user));
    const participantsCount = participants.size;
  
    const sortDates = (dateA, dateB) => dateB - dateA;
    const sortByDateAsc = (msg1, msg2) => new Date(msg1.createdAt) - new Date(msg2.createdAt);
    const lastMessageDate = messages.map(msg => new Date(msg.createdAt)).sort(sortDates)[0];
    const sortedMessages = messages.sort(sortByDateAsc).map(msg => ({ ...msg, createdAt: new Date(msg.createdAt) }) );
    
    return (
      <div className="chat">
          {
          isLoaded 
          ?
            <>
              <Header
                className="chat-header"
                participantsCount={participantsCount}
                messagesCount={messagesCount}
                lastMessageDate={lastMessageDate}
              />
              <MessageList
                messages={sortedMessages}
                currentUser={currentUser.user}
                onSendMessage={newMessage => this.sendMessage(newMessage)}
                onEditMessage={(id, newMessage) => this.editMessage(id, newMessage)}
                onDeleteMessage={id => this.deleteMessage(id)}
                onLikeMessage={(id, userId) => this.likeMessage(id, userId)}
                likeRepository={likeRepository}
              />
            </>
          : <Spinner/>
          }
      </div>
    );
  }
}

export default Chat;
